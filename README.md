# Compile Time Dynamic Language


###### Aim of project: 
Implement a dynamically typed language in C++ that is evaluated 100% at compile time.

###### Approach
The language will be implemented as a tree-walk interpreter which makes use of nodes that  perform different operations.  

###### Compiler
GCC-11 /std:c++20

###### Milestones:
1. Implement basic chainable expressions such as binary ops for float/number types
2. Implement variables using lookup table.
3. Implement boolean and string types.
4. Implement basic error messages for type related errors to expressions.
5. Implement basic control flow through if statements
6. Implement for loops
7. Implement methods
